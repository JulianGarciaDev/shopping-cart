<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Productos_model');
        $this->load->library('pagination');
        $this->load->helper('my_url');
    }

    /**
     * index: Mostrar el listado de articulos en mi carrito de compras
     *
     * @access public
     * @return void
     *
     */
    public function index()
    {
        // Setear titulo de la pagina
        $data['title'] = 'Mi carrito';

        // Cargar vista
        $this->load->view('templates/header', $data);
        $this->load->view('cart', $data);
        $this->load->view('templates/footer', $data);
    }

    /**
     * add: Agregar un articulo al carrito de compras
     *
     * @access public
     * @param  pro_id integer
     * @return void
     *
     */
    public function add($pro_id = NULL)
    {
        if(is_null($pro_id) OR !is_numeric($pro_id))
        {
            // Verifico que venga el producto id, sino muestro 404
            show_404();
        }
        else if(!$this->Productos_model->verify_stock($pro_id))
        {
            // Verifico que el producto tenga stock
            $this->session->set_flashdata('alert_info', 'No hay stock disponible para este producto');
            redirect('producto/' . $pro_id . '/1');
        }
        else if(!is_logged_in())
        {
            // Verifico si el usuario está logueado, sino, creo una variable de session 
            // para que una vez que se loguee lo redireccione a la accion de agregar
            $this->session->set_userdata('s_add', $pro_id);
            // Le muestro un mensaje de ayuda
            $this->session->set_flashdata('alert_info', 'Para continuar inicie sesión');
            // Finalmente lo redirecciono al login
            redirect('user/login');
        }

        // Traigo la información del producto
        $producto = $this->Productos_model->get_producto_detalle($pro_id);

        $data = array(
            'id'      => $producto['pro_id'],
            'qty'     => 1,
            'price'   => $producto['pro_precio'],
            'name'    => $producto['pro_nombre'],
            );

        // Lo agrego al carrito de compras
        $this->cart->insert($data);

        // Creo el mensaje y redirecciono al detalle del producto
        $this->session->set_flashdata('alert_info', 'Se agregó el producto correctamente');
        redirect('producto/' . $producto['pro_id'] . '/' . enhanced_url_title($producto['pro_nombre']));
    }

    /**
     * update_cart: Acutalizar la cantidad de alguno de los productos
     *
     * @access public
     * @return void
     *
     */
    public function update_cart()
    {
        // Validacion de los campos
        $this->form_validation->set_rules('row_id[]', 'columnas', 'required');
        $this->form_validation->set_rules('pro_id[]', 'productos', 'required');
        $this->form_validation->set_rules('qty[]', 'cantidad', 'required|is_natural_no_zero');

        if($this->form_validation->run())
        {
            // Iguale a variable los input en array
            $rows      = $this->input->post('row_id[]');
            $productos = $this->input->post('pro_id[]');
            $cantidad  = $this->input->post('qty[]');

            // Creo variables de respuesta, para luego concatenar
            $errors = '';
            $respuesta = '';

            // Recorro el listado de productos
            foreach ($productos as $k => $pro_id)
            {
                // Traigo el item que se esta recorriendo en este momento
                $item_actual = $this->cart->get_item($rows[$k]);

                // Verifico si la cantidad del item varió. Sino salto el proceso y paso al proximo item
                if($item_actual['qty'] != $cantidad[$k])
                {
                    // Verifico el stock del producto
                    if($this->Productos_model->verify_stock($pro_id, $cantidad[$k]))
                    {
                        $data[] = array(
                            'rowid' => $rows[$k],
                            'qty'   => $cantidad[$k]
                        );
                    }
                    else
                    {
                        $pro_item = $this->Productos_model->get_producto_detalle($pro_id);
                        $errors .= 'El producto ' . $pro_item['pro_nombre'] . ' no tiene stock suficiente (Disponible: ' . $pro_item['pro_stock'] . ') <br>';
                    }
                }
            }
            
            // Si se agrego algo en el array "data", es decir, si se cambio alguna cantidad
            if(isset($data))
            {
                $respuesta = 'Actualizado correctamente ' . count($data) . ' productos';
                $this->cart->update($data);
            }

            $respuesta .= $errors;
        }
        else
        {
            // Errores de validacion del form
            $respuesta = validation_errors();
        }
        
        // Creo mensaje y redirecciono
        $this->session->set_flashdata('alert_info', $respuesta);
        redirect('cart');
    }

    /**
     * delete_cart: Borrar un articulo de mi carrito de compras
     *
     * @access public
     * @return void
     *
     */
    public function delete_cart($row_id = NULL)
    {
        $this->cart->remove($row_id);
        $this->session->set_flashdata('alert_info', 'El producto ha sido eliminado de tu carrito de compras');
        redirect('cart');
    }
}