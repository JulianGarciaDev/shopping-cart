<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Productos_model');
        $this->load->library('pagination');
        $this->load->helper('my_url');
    }

    /**
     * index: Pagina de inicio, muestra listado de articulos
     *
     * @access public
     * @param  limit integer
     * @return void
     *
     */
    public function index($limit = 0)
    {
        // Titulo de la pagina
        $data['title'] = 'Home';

        // Cantidad de productos por pagina
        $per_page = 1;

        // Configuración de la paginación
        $config['base_url'] = site_url('home');
        $config['total_rows'] = count($this->Productos_model->get_productos());
        $config['per_page'] = $per_page;

        // Inicializo paginacion
        $this->pagination->initialize($config);

        // Traigo listado de productos segun los parametros (Paginación, filtros, etc)
        $data['productos'] = $this->Productos_model->get_productos($per_page, $limit);

        // Cargar vistas
        $this->load->view('templates/header', $data);
        $this->load->view('home', $data);
        $this->load->view('templates/footer', $data);
    }

    /**
     * producto_detalle: Mostrar el detalle del producto
     *
     * @access public
     * @param  pro_id integer
     * @return void
     *
     */
    public function producto_detalle($pro_id = NULL)
    {
        if(is_null($pro_id) OR !is_numeric($pro_id))
        {
            // Verifico si hay pro_id, sino muestro 404
            show_404();
        }

        // Busco el producto
        $producto = $this->Productos_model->get_producto_detalle($pro_id);

        if(!$producto)
        {
            // Si el producto no existe, muestro 404
            show_404();
        }

        // Cargo la data de las vistas
        $data['title']    = $producto['pro_nombre'];
        $data['imagenes'] = $this->Productos_model->get_imagenes($pro_id);
        $data['producto'] = $producto;

        // Cargo las vistas
        $this->load->view('templates/header', $data);
        $this->load->view('producto_detalle', $data);
        $this->load->view('templates/footer', $data);
    }

    /**
     * favoritos: Agregar o quitar un producto a favoritos
     *
     * @access public
     * @param pro_id integer
     * @return Void
     *
     */
    public function favorito($pro_id = NULL)
    {
        // Cargo libreria
        $this->load->library('user_agent');

        if(is_null($pro_id) OR !is_numeric($pro_id))
        {
            // Verifico si hay pro_id, sino muestro 404
            show_404();
        }
        else if(!$this->Productos_model->get_producto_detalle($pro_id))
        {
            // Verifico si el producto existe
            $this->session->set_flashdata('alert_info', 'El producto no existe');
        }
        else if(!is_logged_in())
        {
            // Verifico si el usuario esta logueado
            $this->session->set_flashdata('alert_info', 'Debe iniciar sesión para agregar un producto a favoritos');
        }
        else
        {
            // Preparo la data para enviarle al modelo
            $insert = array(
                'pro_id' => $pro_id,
                'use_id' => user_info('use_id')
                );

            // Inserto en base de datos
            if(!$this->Productos_model->set_favorito($insert))
            {
                // Si hubo algun problema muestro mensaje
                $this->session->set_flashdata('alert_error', 'Ocurrió un error al agregar el producto a favoritos. Por favor inténtelo más tarde');
            }
        }

        // Redireccino a la url de donde venia
        redirect($this->agent->referrer());
    }
}