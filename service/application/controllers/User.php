<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->helper('cookie');
        $this->load->library('user_agent');
    }

    /**
     * Login: Funcion para realizar el login de usuarios.
     * Ademas crea y lee cookie de recordarme
     *
     * @access public
     * @return void
     */
    public function login()
    {
        if(is_logged_in())
        {
            redirect('home');
        }
        // Pasar cookies de recordar usuario
        $data['c_email'] = get_cookie('email');

        // Setear las validaciones de los campos de login
        $this->form_validation->set_rules('email', 'nombre de usuario', 'required|valid_email');
        $this->form_validation->set_rules('password', 'contraseña', 'required');

        //Correr validación
        if($this->form_validation->run())
        {
            // Igualo a variables los post realizados
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $remember = $this->input->post('rememberme');

            // Verificar intentos de fuerza bruta
            if($this->verify_attempts($email))
            {
                return show_message('Has superado el numero de intentos de login. Inténtelo más tarde', 'info', 401);
            }

            // Hasheo Password
            $password_hashed = hash('sha256', $password);

            // Verifico si el usuario existe
            if($user = $this->User_model->get_login($email, $password_hashed))
            {
                // Verifico si quiere recordar su usuario
                if($remember)
                {
                    // Creo cookies por 1 año si es que quiere recordar su usuario
                    set_cookie('email', $email, 525600*60);
                }
                else
                {
                    // Borro cookie de recordar si está destildada la opción
                    delete_cookie('email');
                }

                // Crear sesion: Se crea variable por variable ya que por array da error
                $this->session->set_userdata('use_id', $user['use_id']);
                $this->session->set_userdata('use_username', $user['use_username']);
                $this->session->set_userdata('use_email', $user['use_email']);
                $this->session->set_userdata('use_token', $this->create_user_token());

                // Setear log de login
                $this->User_model->set_log_login('login');

                // Login correcto, redirecciono
                if($pro_id = $this->session->userdata('s_add'))
                {
                    // Si viene desde la accion de compra
                    redirect('cart/add/' . $pro_id);
                }
                else
                {
                    // Sino, redirecciono al home
                    redirect('home');
                }
            }
            else
            {
                // Error: usuario no encontrado o contraseña incorrecta
                $this->User_model->set_log_attempts($email);
                $data['alert_error'] = 'Datos incorrectos';
            }
        }
        else
        {
            // Error: errores de validación
            $data['alert_error'] = validation_errors();
        }

        // Cargar vistas
        $this->load->view('templates/header', $data);
        $this->load->view('user/login', $data);
        $this->load->view('templates/footer', $data);
    }

    /**
     * Forgot_password: Funcion para recuperar contraseña.
     * Solicita email del usuario, crea un token el cual es enviado por email
     *
     * @access public
     * @return void
     */
    public function forgot_password()
    {
        if(is_logged_in())
        {
            return redirect();
        }

        $this->form_validation->set_rules('email', 'email', 'required|valid_email');

        if($this->form_validation->run())
        {
            $email = $this->input->post('email');

            // Traigo la información del usuario
            $user = $this->User_model->get_user_by_email($email);

            // Verifico si el usuario existe
            if($user)
            {
                $token = $this->create_forgot_token($email);
                if($token)
                {
                    $email_data['token'] = $token;

                    send_email('Recuperar contraseña', 'user/emails/forgot_password-html', $user['use_email'], $email_data);

                    // Mensaje de login correcto
                    return $this->show_message('Te enviamos un correo para recuperar tu contrseña.', 'info', 401);
                }
                else
                {
                    $data['alert_error'] = 'Ocurrió un error al generar la recuperación, por favor intentelo más tarde';    
                }
            }
            else
            {
                // Error: usuario no encontrado
                $data['alert_error'] = 'El Email no está registrado';
            }
        }
        else
        {
            // Error: errores de validación
            $data['alert_error'] = validation_errors();
        }

        $this->load->view('user/forgot_password', $data);
    }

    /**
     * reset_password: Funcion para reiniciar el password, 
     * linkeado previamente desde el mail de recuperar contraseña.
     * Verifica el token con respecto al que está en base de datos
     *
     * @access public
     * @param  token string
     * @return void
     */
    public function reset_password($token = NULL)
    {
        // Verifico que venga token
        if(is_null($token))
        {
            // Sino, muestro 404
            show_404();
        }

        $token         = base64_decode($token); // Decodifico el token en base64
        $token_decode  = $this->encrypt->decode($token); // Lo desencripto con la key que esten en el archivo de config
        $token_explode = explode(',', $token_decode); // Separo el string en comas y lo transformo en array
        $email         = $token_explode[0]; // Tomo el item 0 del array que es el email
        $token_ok      = $this->User_model->verify_forgot_token($email, $token); // Verifico si el email y el token coinciden con el de base de datos

        // Si la combinación toke-email no es correcta, muestro 404
        if(!$token_ok)
        {
            show_404();
        }

        // Validacion para los campos de cambio de contraseña
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'contraseña', 'required');
        $this->form_validation->set_rules('password_confirm', 'repetir contraseña', 'required|matches[password]');

        if($this->form_validation->run())
        {
            $use_email    = $this->input->post('email');
            $use_password = $this->input->post('password');

            // Hasheo la contraseña
            $password_hashed = hash('sha256', $use_password);

            // Verifico que el email ingresado sea igual al del parametro "token"
            if($email == $this->input->post('email'))
            {
                // Cambio la contraseña
                $res = $this->User_model->update_password($use_email, $password_hashed);

                if($res)
                {
                    $data['alert_success'] = 'La contraseña se cambio correctamente, por favor inicie sesión';
                }
                else
                {
                    $data['alert_error'] = 'Ocurrió un error al cambiar la contraseña, por favor intentelo más tarde';
                }
            }
            else
            {
                $data['alert_error'] = 'El email ingresado no es correcto';    
            }
        }
        else
        {
            // Error: errores de validación
            $data['alert_error'] = validation_errors();
        }
        // Cargar vista
        $this->load->view('user/reset_password', $data);
    }

    /**
     * register: Funcion para registrar usuarios, al finalizar inicia sesión
     *
     * @access public
     * @return void
     *
     */
    public function register()
    {
        if(is_logged_in())
        {
            redirect();
        }

        $this->form_validation->set_rules('username', 'nombre de usuario', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check');
        $this->form_validation->set_rules('password', 'contraseña', 'required');
        $this->form_validation->set_rules('password_confirm', 'repetir contraseña', 'required|matches[password]');

        if($this->form_validation->run())
        {
            $use_email = $this->input->post('email');

            $user_data = array(
                'use_username' => $this->input->post('username'),
                'use_email'    => $use_email,
                'use_password' => hash('sha256', $this->input->post('password')),
                'use_register' => date('Y-m-d H:i:s'),
                );

            // Creo el nuevo usuario
            $register = $this->User_model->set_user($user_data);

            if($register)
            {
                // Traer información del usuario registrado
                $user = $this->User_model->get_user_by_email($use_email);

                // Crear sesión
                $this->session->set_userdata('use_id', $register);
                $this->session->set_userdata('use_username', $user['use_username']);
                $this->session->set_userdata('use_email', $user['use_email']);
                $this->session->set_userdata('use_token', $this->create_user_token());

                // Setear log de login
                $this->User_model->set_log_login('registro', $user['use_id']);

                // Registro y login correcto, redirecciono
                if($pro_id = $this->session->userdata('c_add'))
                {
                    // Si viene desde la accion de compra
                    redirect('cart/add/' . $pro_id);
                }
                else
                {
                    // Sino, redirecciono al home
                    redirect('home');
                }
            }
            else
            {
                // Error de base de datos al registrar
                $data['alert_error'] = 'Ocurrió un error al registrar el usuario';
            }
        }
        else
        {
            // Error: errores de validación
            $data['alert_error'] = validation_errors();
        }

        // Cargar vistas
        $this->load->view('user/register', $data);
    }

    /**
     * logout: Cerrar sesión
     *
     * @access public
     * @return redirect
     *
     */
    public function logout()
    {
        // Setear log de logout
        $this->User_model->set_log_login('logout');

        // Destruir la sesión
        $this->session->sess_destroy();

        // Redirecciono al home del sitio
        redirect();
    }

    /**
     * email_check: Verificar que el email no este registrado
     *
     * @access public
     * @return bool
     *
     */
    function email_check($email)
    {
        if ($this->User_model->get_user_by_email($email))
        {
            $this->form_validation->set_message('email_check', 'La dirección Email ya está en uso');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    /**
     * create_user_token: Crear token de usuario. Cada vez que se loguee se creará uno nuevo
     *
     * @access private
     * @return string
     *
     */
    private function create_user_token()
    {
        // Crear token: Fecha actual, numero random del 0 al 99999, hasheado en sha256
        $token = date('Y-m-d H:i:s') . rand(0, 99999);
        $token = hash('sha256', $token);

        // Actualizar el token en la tabla de usuarios
        $this->User_model->set_user_token($token);

        return $token;
    }

    /**
     * create_forgot_token: Token de recuperación de contraseña
     *
     * @access private
     * @param  email string
     * @return string
     *
     */
    private function create_forgot_token($email = NULL)
    {
        // Crear token: Email del usuario, Fecha actual, numero random del 0 al 99999
        $token = $email . ',' . date('Y-m-d H:i:s') . ',' . rand(0, 99999);
        // Lo encripto con la key que se encuentra en el archivo config
        $token = $this->encrypt->encode($token);

        // Fecha de expiración del token, en 24 horas
        $datetime   = new DateTime('24 hours');
        $expiration = $datetime->format('Y-m-d H:i:s');

        // Actualizo el token y la fecha de expiración en la tabla de usuarios
        if($this->User_model->set_forgot_token($token, $expiration, $email))
        {
            // Codifico el token en base 64 y le quito los sigos "=" para ser enviado por mail
            return str_replace('=', '', base64_encode($token));
        }
        
        return false;
    }

    /**
     * verify_attempts: Verifica la cantidad de intentos fallidos de ese usuario
     *
     * @access private
     * @param  use_email string
     * @return bool
     *
     */
    private function verify_attempts($use_email = NULL)
    {
        // Obtengo la IP del usuario que se intenta loguear
        $att_ip = $this->input->ip_address();

        // Busco los intentos de ese usuario o de esa IP
        $attempts = $this->User_model->get_log_attempts($att_ip, $use_email);

        // Si la cantidad registrada en la base de datos es mayor o igual a la cantidad seteada en el archivo de configuración
        if($attempts['att_count'] >= $this->config->item('attempts_limit'))
        {
            return true;
        }

        return false;
    }
}