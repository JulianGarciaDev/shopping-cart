<?php

function get_csrf($item = 'name')
{
	$ci = & get_instance();

	$csrf = array(
	        'name' => $ci->security->get_csrf_token_name(),
	        'hash' => $ci->security->get_csrf_hash()
	);

	return $csrf[$item];
}
