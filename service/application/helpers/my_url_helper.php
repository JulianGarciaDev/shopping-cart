<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('enhanced_url_title'))
{
	function enhanced_url_title($str, $separator = 'dash', $lowercase = TRUE)
	{
		$separator = ($separator == 'dash') ? '-' : '_';
		$str = strip_tags($str);
		$from = array(' ','ă','á','à','â','ã','ª','Á','À',
		  'Â','Ã', 'é','è','ê','É','È','Ê','í','ì','î','Í',
		  'Ì','Î','ò','ó','ô', 'õ','º','Ó','Ò','Ô','Õ','ş','Ş'
		  ,'ţ','Ţ','ü','ú','ù','û','Ü','Ú','Ù','Û','ç','Ç','Ñ','ñ');
		$to = array($separator,'a','a','a','a','a','a','A','A',
		  'A','A','e','e','e','E','E','E','i','i','i','I','I',
		  'I','o','o','o','o','o','O','O','O','O','s','S',
		  't','T','u','u','u','u','U','U','U','U','c','C','N','n');
		$str = trim(str_replace($from, $to, $str));
		$str = preg_replace('/[^A-Za-z0-9\-]/', '', $str);
		if ($lowercase === TRUE)
		  $str = strtolower($str);
		return $str;
	}

	function url_tag_decode($string)
	{
		return str_replace('-', ' ', urldecode($string));
	}

	function url_tag_encode($string)
	{
		return str_replace('+', '-', urlencode($string));
	}
}