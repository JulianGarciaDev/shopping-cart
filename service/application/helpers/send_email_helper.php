<?php

function send_email($subject = NULL, $template = NULL, $to = NULL, $data = NULL, $text = NULL)
{
	$CI = & get_instance();

	$msg = $text;
	
	if($template != NULL)
	{
		$msg = $CI->load->view($template, $data, true);
	}
	
	$CI->load->library('email');

	$CI->email->from('prueba@juliangarcia.com.ar');
	$CI->email->to($to);
	$CI->email->subject($subject);
	$CI->email->message($msg);
	
	return $CI->email->send();
}