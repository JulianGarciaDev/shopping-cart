<?php 

function user_info($item = NULL)
{
	$ci = &get_instance();

	$user_info = array(
		'use_id'       => $ci->session->userdata('use_id'),
		'use_email'    => $ci->session->userdata('use_email'),
		'use_username' => $ci->session->userdata('use_username'),
		'use_token'    => $ci->session->userdata('use_token')
		);

	if($item != NULL)
	{
		return $user_info[$item];
	}

	return $user_info;
}

function is_logged_in()
{
	$ci = &get_instance();

	$use_id       = user_info('use_id');
	$use_email    = user_info('use_email');
	$use_username = user_info('use_username');
	$use_token    = user_info('use_token');

	if($use_id && $use_email && $use_username && $use_token)
	{
		$ci->load->model('User_model');

		$token_ok = $ci->User_model->verify_token($use_id, $use_token);
		
		if($token_ok)
		{
			return true;
		}
	}

	return false;
}


function is_admin()
{
	$ci = &get_instance();

	if(is_logged_in())
	{
		$use_id = $ci->session->userdata('use_id');

		$ci->load->model('User_model');

		$admin_ok = $ci->User_model->verify_admin($use_id);
		if($admin_ok)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	return false;
}

function show_message($text = NULL, $type = NULL, $header = NULL, $return_to = NULL)
{
	$ci = &get_instance();

    $data['alert_text'] = $text;
    $data['alert_type'] = $type;
    $data['return_to']  = $return_to;

    if($header != NULL)
    {
        $ci->output->set_status_header($header);
    }

    $ci->load->view('message', $data);
}