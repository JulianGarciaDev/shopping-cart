<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos_model extends CI_Model {

	function get_productos($per_page = NULL, $limit = NULL)
	{
		// Paginacion
		if($limit != NULL OR $limit == 0)
		{
			$this->db->limit($per_page, $limit);
		}
		
		// Verifico si el usuario está logueado
		if($use_id = user_info('use_id'))
		{
			// Hago sub consultar para traer el estado de favorito entre el usuario y el producto
			$this->db->select("(SELECT fav.fav_estado FROM fav_favoritos fav WHERE fav.pro_id = pro.pro_id AND fav.use_id = '$use_id') AS fav_estado,");
		}

		// Columnas a seleccionar
		$this->db->select('pro.*, img.img_url, cat.cat_id, cat.cat_nombre');
		// Join entre categorias y categorias por productos
		$this->db->join('cxp_catxproducto AS cxp', 'cxp.pro_id = pro.pro_id');
		// Join entre productos y categorias por productos
		$this->db->join('cat_categorias AS cat', 'cat.cat_id = cxp.cat_id');
		// Join Imagenes de productos
		$this->db->join('img_imagenes_productos AS img', 'img.pro_id = pro.pro_id');
		// El producto debe estar activo
		$this->db->where('pro.pro_estado', 1);
		// Agrupar por producto
		$this->db->group_by('pro.pro_id');
		// Orden
		$this->db->order_by('pro.pro_orden DESC, pro.pro_id DESC');
		// Tabla principal
		$res = $this->db->get('pro_productos AS pro');

		return $res->result_array();
	}

	function get_producto_detalle($pro_id = NULL)
	{
		// Verifico si el usuario está logueado
		if($use_id = user_info('use_id'))
		{
			// Hago sub consultar para traer el estado de favorito entre el usuario y el producto
			$this->db->select("(SELECT fav.fav_estado FROM fav_favoritos fav WHERE fav.pro_id = pro.pro_id AND fav.use_id = '$use_id') AS fav_estado,");
		}

		// Columnas a seleccionar
		$this->db->select('pro.*, cat.cat_id, cat.cat_nombre');
		// Join entre categorias y categorias por productos
		$this->db->join('cxp_catxproducto AS cxp', 'cxp.pro_id = pro.pro_id');
		// Join entre productos y categorias por productos
		$this->db->join('cat_categorias AS cat', 'cat.cat_id = cxp.cat_id');
		// Join Imagenes de productos
		$this->db->join('img_imagenes_productos AS img', 'img.pro_id = pro.pro_id');
		// El producto debe estar activo
		$this->db->where('pro.pro_estado', 1);
		// Traer el producto que viene por parametro
		$this->db->where('pro.pro_id', $pro_id);
		// Agrupar por producto
		$this->db->group_by('pro.pro_id');
		// Tabla principal
		$res = $this->db->get('pro_productos AS pro');

		return $res->row_array();
	}

	function get_imagenes($pro_id = NULL)
	{
		$this->db->select('img.img_url');
		$this->db->order_by('img.img_orden DESC');
		$res = $this->db->get_where('img_imagenes_productos AS img', array('pro_id' => $pro_id));

		return $res->result_array();
	}

	function verify_stock($pro_id = NULL, $stock = 1)
	{
		$this->db->where('pro.pro_estado', 1);
		$this->db->where('pro.pro_stock >', $stock);
		$this->db->where('pro.pro_id', $pro_id);
		$res = $this->db->get('pro_productos AS pro');
		
		return $res->row_array();
	}

	function set_favorito($data = NULL)
	{
		// Verifico si el usuario tiene ya un registro para ese producto favorito
		if($res = $this->get_favorito($data))
		{
			// Si lo tiene, me fijo en el estado que tiene actualmente y lo convierto al contrario
			if($res['fav_estado'] == 1)
			{
				$fav_estado = 0;
			}
			else
			{
				$fav_estado = 1;
			}
			// Actualizo el registro
			return $this->toggle_favorito($data, $fav_estado);
		}
		else
		{
			// Si el usuario no tiene registro de ese producto favorito (sea el estado que sea), lo inserto
			return $this->db->insert('fav_favoritos', $data);
		}
	}

	function get_favorito($data = NULL)
	{
		$res = $this->db->get_where('fav_favoritos', $data);
		return $res->row_array();
	}

	function toggle_favorito($data, $fav_estado = 1)
	{
		$this->db->where($data);
		return $this->db->update('fav_favoritos', array('fav_estado' => $fav_estado));
	}
}