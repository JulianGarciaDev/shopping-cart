<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	function get_login($email = NULL, $password = NULL)
	{
		$res = $this->db->get_where('use_users', array('use_email' => $email, 'use_password' => $password));
		return $res->row_array();
	}

	function get_user_by_email($email = NULL)
	{
		$this->db->select('use_email, use_username');
		$res = $this->db->get_where('use_users', array('use_email' => $email));
		return $res->row_array();
	}

	function set_user($user_data = NULL)
	{
		$res = $this->db->insert('use_users', $user_data);
		return $this->db->insert_id();
	}

	function set_user_token($token = NULL)
	{
		$use_id = $this->session->userdata('use_id');
		
		if($use_id)
		{
			$this->db->where('use_id', $use_id);
			return $this->db->update('use_users', array('use_token' => $token));
		}

		return false;
	}

	function verify_token($use_id = NULL, $use_token = NULL)
	{
		$res = $this->db->get_where('use_users', array('use_id' => $use_id, 'use_token' => $use_token));
		return $res->row_array();
	}

	function set_forgot_token($token = NULL, $expire = NULL, $use_email = NULL)
	{
		$this->db->where('use_email', $use_email);
		return $this->db->update('use_users', array('use_forgot_token' => $token, 'use_forgot_expire' => $expire));

		return false;
	}

	function verify_forgot_token($use_email = NULL, $use_token = NULL)
	{
		$now_date = date('Y-m-d H:i:s');

		$this->db->where("use_forgot_expire > '$now_date'");
		$res = $this->db->get_where('use_users', array('use_forgot_token' => $use_token, 'use_email' => $use_email));
		return $res->row_array();
	}

	function verify_admin($use_id = NULL)
	{
		$res = $this->db->get_where('use_users', array('use_id' => $use_id, 'use_admin' => 1));
		return $res->row_array();
	}


	function update_password($use_email = NULL, $use_password = NULL)
	{
		$this->clear_log_attempts($use_email);

		$this->db->where('use_email', $use_email);
		return $this->db->update('use_users', array('use_password' => $use_password, 'use_forgot_token' => NULL, 'use_forgot_expire' => NULL));
	}

	function set_log_login($login_action = NULL, $use_id = NULL)
	{
		if($use_id == NULL)
		{
			$use_id = $this->session->userdata('use_id');
		}

		$login_data = array(
			'use_id'       => $use_id,
			'login_ip'     => $this->input->ip_address(),
			'login_action' => $login_action,
			'login_agent'  => $this->agent->agent_string()
			);
		return $this->db->insert('log_login', $login_data);
	}

	function set_log_attempts($use_email = NULL)
	{
		// Ip que realizó el intento de login
		$att_ip = $this->input->ip_address();
		
		// Agrego tiempo de expiración
		$expire = strtotime('now') + $this->config->item('attempts_expire');

		$attempts_data = array(
			'att_ip'     => $att_ip,
			'att_expire' => date('Y-m-d H:i:s', $expire),
			);

		// Traer intentos previos
		$prev_attempts = $this->get_log_attempts($att_ip, $use_email);

		// Si existen intentos anteriores y el numero es menor al limite
		if($prev_attempts)
		{
			// Incremento en 1 el contador de intentos
			$attempts_data['att_count'] = $prev_attempts['att_count'] + 1;

			$this->db->where('att_id', $prev_attempts['att_id']);
			$this->db->where("(att_email = '$use_email' OR att_ip = '$att_ip')");
			return $this->db->update('log_attempts', $attempts_data);
		}
		else
		{
			// Sino inserto un nuevo intento
			$attempts_data['att_count'] = 1;
			$attempts_data['att_email'] = $use_email;
			return $this->db->insert('log_attempts', $attempts_data);
		}

		return false;
	}

	function get_log_attempts($att_ip = NULL, $att_email = NULL)
	{
		$now_date = date('Y-m-d H:i:s');

		// Verfico si existen intentos previos
		$this->db->where("(att_email = '$att_email' OR att_ip = '$att_ip')");
		$this->db->where("att_expire > '$now_date'");
		$res = $this->db->get('log_attempts');

		return $res->row_array();
	}

	function clear_log_attempts($use_email = NULL)
	{
		$this->db->where('att_email', $use_email);
		$this->db->where('att_ip', $this->input->ip_address());
		$this->db->delete('log_attempts');
	}
}