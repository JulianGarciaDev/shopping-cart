<form method="POST" action="<?php echo site_url('cart/update_cart'); ?>" autocomplete="off">
<?php if($this->cart->total_items() == 0) { ?>
    No has agregado ningun producto al carrito
<?php 
    }
    else
    {
        $i = 1;
        foreach ($this->cart->contents() as $row_id => $cart_item): 
?>
            <div>
                <?php echo $i ?>
                | Nombre: <?php echo $cart_item['name'] ?>
                | Precio: $<?php echo $this->cart->format_number($cart_item['price']); ?>
                | Cantidad: <input type="number" name="qty[]" id="qty[]" value="<?php echo $cart_item['qty']; ?>">
                | Subtotal: $<?php echo $this->cart->format_number($cart_item['subtotal']); ?>
                | <a href="<?php echo site_url('cart/delete_cart/' . $row_id) ?>">Eliminar</a>
            </div>
            <input type="hidden" name="row_id[]" value="<?php echo $row_id ?>">
            <input type="hidden" name="pro_id[]" value="<?php echo $cart_item['id'] ?>">
<?php
            $i++;
        endforeach; 
    }
?>
<hr />
Total: $<?php echo $this->cart->format_number($this->cart->total()); ?>
 | <button type="submit">Actualizar</button>
 | <a href="<?php echo site_url('pagar'); ?>">Pagar</a>
<input type="hidden" name="<?php echo get_csrf('name'); ?>" value="<?php echo get_csrf('hash'); ?>">
</form>
