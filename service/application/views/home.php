<!-- Productos -->
<?php foreach ($productos as $pro_item): ?>
	<article>
		<h3><?php echo $pro_item['pro_nombre'] ?></h3>
		<figure>
			<img src="<?php echo site_url('static/images/productos/' . $pro_item['img_url']); ?>" width="100px" />
		</figure>
		<p><?php echo character_limiter($pro_item['pro_descripcion'], 20) ?></p>
		<p>
			<strong>$<?php echo $pro_item['pro_precio'] ?></strong>
		</p>
		<p>
			<?php echo $pro_item['cat_nombre'] ?>
			<?php if($pro_item['pro_destacado'] == 1) echo ' | Destacado'; ?>
		</p>
		<a href="<?php echo site_url('producto/' . $pro_item['pro_id'] . '/' . enhanced_url_title($pro_item['pro_nombre'])); ?>">Ver más</a> | 
		<?php if($pro_item['pro_stock'] != 0): ?>
			<a href="<?php echo site_url('cart/add/' . $pro_item['pro_id']) ?>">+ Agregar al carrito</a>
		<?php endif; ?>
		
		<?php if(user_info('use_id')): ?>
			 | 
			<a href="<?php echo site_url('home/favorito/' . $pro_item['pro_id']) ?>">
				<?php if($pro_item['fav_estado'] == 1): ?>
					Quitar de favoritos
				<?php else: ?>
					Agregar a favoritos
				<?php endif; ?>
			</a>
		<?php endif; ?>
		<hr />
	</article>
<?php endforeach; ?>
<!-- End Productos -->

<!-- Pagination -->
<?php echo $this->pagination->create_links(); ?>
<!-- End Pagination -->