<?php if(isset($alert_text)): ?>
	<div class="<?php echo $alert_type ?>"><?php echo $alert_text; ?></div>
<?php endif; ?>

<?php if(isset($return_to)): ?>
	<div class="<?php echo site_url('$return_to'); ?>">Aceptar</div>
<?php endif; ?>