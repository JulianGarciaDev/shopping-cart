<article>
	<h3><?php echo $producto['pro_nombre'] ?></h3>
	<figure>
		<img src="<?php echo site_url('static/images/productos/' . $imagenes[0]['img_url']); ?>" width="100px" />
	</figure>
	<p><?php echo $producto['pro_descripcion'] ?></p>
	<p>
		<strong>$<?php echo $producto['pro_precio'] ?></strong>
	</p>
	<p>
		<?php echo $producto['cat_nombre'] ?>
		<?php if($producto['pro_destacado'] == 1) echo ' | Destacado'; ?>
	</p>

	<?php if($producto['pro_stock'] == 0): ?>
		<a href="<?php echo site_url('cart/add/' . $producto['pro_id']) ?>">+ Agregar al carrito</a>
	<?php else: ?>
		<p>Sin Stock</p>
	<?php endif; ?>

	<?php if(user_info('use_id')): ?>
		 | 
		<a href="<?php echo site_url('home/favorito/' . $producto['pro_id']) ?>">
			<?php if($producto['fav_estado'] == 1): ?>
				Quitar de favoritos
			<?php else: ?>
				Agregar a favoritos
			<?php endif; ?>
		</a>
	<?php endif; ?>
</article>
<hr />
<a href="<?php echo site_url('home'); ?>"><< Mas productos</a>