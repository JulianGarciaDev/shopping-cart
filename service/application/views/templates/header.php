<!DOCTYPE html>
<html>
<head>
	<title><?php if(isset($title)) echo $title . ' | ' ?>Shopping cart</title>
	<meta charset="utf-8">
</head>
<body>
	<a href="<?php echo site_url(); ?>">Home</a>
	<?php if(!is_logged_in()): ?>
		 | <a href="<?php echo site_url('user/login'); ?>">Login</a>
		 | <a href="<?php echo site_url('user/register'); ?>">Registrar</a> 
	<?php else: ?>
		 | Bienvenido <?php echo user_info('use_username'); ?>
		 | <a href="<?php echo site_url('cart'); ?>">Mi carrito (<?php echo $this->cart->total_items() ?>)</a>
		 | <a href="<?php echo site_url('user/logout'); ?>">Logout</a>
	<?php endif; ?>
	<br />
	<div><?php if($this->session->flashdata('alert_info')) echo $this->session->flashdata('alert_info'); ?></div>
	<div><?php if($this->session->flashdata('alert_error')) echo $this->session->flashdata('alert_error'); ?></div>
	<div><?php if($this->session->flashdata('alert_success')) echo $this->session->flashdata('alert_success'); ?></div>
	<br />