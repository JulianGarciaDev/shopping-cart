<p><?php if(isset($alert_error)) echo $alert_error; ?></p>
<p><?php if(isset($alert_success)) echo $alert_success; ?></p>
<form method="POST" autocomplete="off">
	<input type="email" name="email" value="<?php if(isset($c_email)) echo $c_email ?>" placeholder="Email">
	<br />
	<input type="password" name="password" id="password" placeholder="Password">
	<br />
	<label>
		<input type="checkbox" name="rememberme" <?php if(isset($c_email)) echo 'checked'; ?>>
		Recordar mi usuario
	</label>
	<br />
	<input type="hidden" name="<?php echo get_csrf('name'); ?>" value="<?php echo get_csrf('hash'); ?>">
	<button type="submit">Ingresar</button>
	<p><a href="<?php echo site_url('user/forgot_password'); ?>">Olvidé mi contraseña</a></p>
	<p><a href="<?php echo site_url('user/register'); ?>">Registrate</a></p>
</form>