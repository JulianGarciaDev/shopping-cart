-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-06-2016 a las 01:30:34
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `shopping-cart`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_categorias`
--

CREATE TABLE IF NOT EXISTS `cat_categorias` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_nombre` varchar(255) NOT NULL,
  `cat_padre` int(11) NOT NULL DEFAULT '0',
  `cat_orden` int(11) NOT NULL DEFAULT '0',
  `cat_estado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cxp_catxproducto`
--

CREATE TABLE IF NOT EXISTS `cxp_catxproducto` (
  `cxp_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `pro_id` int(11) NOT NULL,
  PRIMARY KEY (`cxp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fav_favoritos`
--

CREATE TABLE IF NOT EXISTS `fav_favoritos` (
  `fav_id` int(11) NOT NULL AUTO_INCREMENT,
  `pro_id` int(11) NOT NULL,
  `use_id` int(11) NOT NULL,
  `fav_estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`fav_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `img_imagenes_productos`
--

CREATE TABLE IF NOT EXISTS `img_imagenes_productos` (
  `img_id` int(11) NOT NULL AUTO_INCREMENT,
  `img_url` varchar(255) NOT NULL,
  `pro_id` varchar(255) NOT NULL,
  `img_orden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`img_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log_attempts`
--

CREATE TABLE IF NOT EXISTS `log_attempts` (
  `att_id` int(11) NOT NULL AUTO_INCREMENT,
  `att_expire` datetime NOT NULL,
  `att_ip` varchar(50) NOT NULL,
  `att_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `att_count` int(11) NOT NULL,
  `att_email` varchar(255) NOT NULL,
  PRIMARY KEY (`att_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log_login`
--

CREATE TABLE IF NOT EXISTS `log_login` (
  `login_id` int(11) NOT NULL AUTO_INCREMENT,
  `use_id` int(11) NOT NULL,
  `login_ip` varchar(255) NOT NULL,
  `login_action` varchar(50) NOT NULL,
  `login_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `login_agent` varchar(255) NOT NULL,
  PRIMARY KEY (`login_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pro_productos`
--

CREATE TABLE IF NOT EXISTS `pro_productos` (
  `pro_id` int(11) NOT NULL AUTO_INCREMENT,
  `pro_nombre` varchar(255) DEFAULT NULL,
  `pro_descripcion` text,
  `pro_precio` float NOT NULL DEFAULT '0',
  `pro_estado` tinyint(1) NOT NULL DEFAULT '0',
  `pro_stock` int(11) NOT NULL DEFAULT '0',
  `pro_fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pro_destacado` tinyint(1) NOT NULL DEFAULT '0',
  `pro_orden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pro_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `use_users`
--

CREATE TABLE IF NOT EXISTS `use_users` (
  `use_id` int(11) NOT NULL AUTO_INCREMENT,
  `use_username` varchar(255) NOT NULL,
  `use_email` varchar(255) NOT NULL,
  `use_password` varchar(255) NOT NULL,
  `use_forgot_token` varchar(255) DEFAULT NULL,
  `use_forgot_expire` timestamp NULL DEFAULT NULL,
  `use_register` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `use_admin` tinyint(1) DEFAULT NULL,
  `use_token` varchar(255) NOT NULL,
  PRIMARY KEY (`use_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
